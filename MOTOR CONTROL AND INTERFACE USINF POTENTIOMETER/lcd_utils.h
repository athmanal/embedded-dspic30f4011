#ifndef __LCD_UTILS__
#define __LCD_UTILS__

#include "project.h"

void lcd_init();
unsigned int8 lcd_write(unsigned int8 str_length, char* str_pointer);
unsigned int8 lcd_write_char_at(unsigned int8 line, unsigned int8 char_pos, unsigned int8 char_to_write);
unsigned int8 lcd_write_at(unsigned int8 line, unsigned int8 char_pos, unsigned int8 str_length, char* str_pointer) ;
#endif