% Luca Parolini
% demo code to read data from the serial port in Matlab
clear all
close all
clc;        

num_rows = 15000;
num_cols = 2;

data = zeros(num_rows, num_cols); % data vector
serial_port_name = 'COM1';
s = serial(serial_port_name);
% set specific parameters
s.Baudrate = 9600;
s.DataBits = 8;
s.FlowControl = 'none';
s.Parity = 'none';
s.StopBits = 1;
s.InputBufferSize = num_rows*num_cols*2;    % 16 bit data 2 cols
disp('Serial port created. Press Enter to open the serial port');
pause; % waiting

fopen(s); % open file for reading
% wait data for data in the serial port buffer
pause(1);
counter = 0;
no_data = 0;
disp('Reading data ...');
while counter < num_rows
    if s.BytesAvailable == 0
        if no_data == 1
            break;     % leave the cicle no more data available
        end
        disp('Waiting new data');
        pause(5);   % wait for new data
        no_data = 1;    % set new state
        continue;
    end
    if no_data == 1
        disp('Reading data ...');
    end
    no_data = 0;     % some data read
    tmp = fgetl(s);      % read text data
    [data(counter+1,1) data(counter+1,2)]= strread(tmp,'%d%d');
    counter = counter + 1;
    %pause(0.2);    % wait between one line and the other
end
fclose(s);
disp(['Read ' num2str(counter) ' lines']);
