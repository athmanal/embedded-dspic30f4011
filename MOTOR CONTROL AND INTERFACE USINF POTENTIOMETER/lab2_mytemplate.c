
#include "project.h"
#include "lcd_utils.h"

#include <stdio.h>
#include <string.h>

#use delay( internal=7.37mhz)
#fuses nowdt, noprotect       //Turn off the Watch-Dog Timer and disable code protection
#use rs232(baud=9600, PARITY=N, STOP=1, UART1A)
#use standard_io(b)
#use standard_io(f)
#use standard_io(e)

// Convenience defines
#define DIR     PIN_F0

// TODO: Define any globals here 
int value;
int duty;
int interrupt0; 
int interrupt1; 
int timerInterrupt;
long prevQuadcount;
long quadcount;
long diff; 
int rpm;
long ticksPerMin;


char clear[10] = {"         "};
char lcd_txt[32];

void write_lcd(int value){
   sprintf(lcd_txt,"%d\r\n",value);
      int lcd_txt_length = strlen(lcd_txt);
      lcd_write_at(2,0,lcd_txt_length, lcd_txt);
}

void clear_lcd(){
      int lcd_txt_length = strlen(clear);
      lcd_write_at(2,0,lcd_txt_length, clear);
}



void main (void)
{   
   //Setup I/O
   set_tris_b(0xff);
   set_tris_f(0x00);

   //   ENABLE INTERRUPTS
   enable_interrupts(INT_TIMER2);
   setup_timer2(TMR_INTERNAL,0x46b4);

   //INITIALIZE LCD
   lcd_init();               // initialize lcd communication (SPI)
   memset(lcd_txt,'\0', 32);      // clear the serial buffer

      //SETUP ADC CHANNEL;
    setup_adc(ADC_CLOCK_INTERNAL);
    setup_adc_ports(sAN1 | sAN2 );
    set_adc_channel(2); 
    delay_us(10);

      //SETUP QEI
    setup_qei(QEI_MODE_X4|QEI_TIMER_INTERNAL,QEI_FILTER_DIV_1,QEI_FORWARD);
    quadcount = 0;
    prevQuadcount = 0;


      while(1)  
          {  
			
			write_lcd(rpm); 
			printf("rpm = %d \r\n",rpm);
			delay_ms(250);
			clear_lcd();
			 }


}


   #INT_TIMER2
   void timer_2()
   {
   	  if( value ==0)
      {
         output_high(DIR);   
         value = 1;
      }
      else 
      {
         output_low(DIR);
         value = 0;
      }

      prevQuadcount = quadcount;
      quadcount = qei_get_count();// Read the count. 
	  diff = quadcount - prevQuadcount;
      ticksPerMin = abs(diff)/0.000333333;
	  rpm = ticksPerMin/2048;
      clear_interrupt(INT_TIMER2);
   }
