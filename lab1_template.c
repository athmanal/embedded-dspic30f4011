// S15, 18-474
// Luca Parolini <lparolin@andrew.cmu.edu>
// 01/23/10
// Updated by Akshay Rajhans <arajhans@ece.cmu.edu> during the S11 semester.
// Updated by Nikos Arechiga <narechig@ece.cmu.edu> during the S12 semester
// Updated by Ben Wasserman <bwasserm@andrew.cmu.edu> during the S13 semester.
// Updated by Chris Leaf <cleaf@andrew.cmu.edu> during the S15 semester.

#include "project.h"

#include "lcd_utils.h"

#include <stdio.h>
#include <string.h>

#use delay( internal=7.37mhz)
#fuses nowdt, noprotect       //Turn off the Watch-Dog Timer and disable code protection
#use rs232(baud=9600, PARITY=N, STOP=1, UART1A)
#use standard_io(b)
#use standard_io(f)
#use standard_io(e)

// Convenience defines
#define TOGPIN PIN_B3
#define INP_0   PIN_F0
#define INP_1   PIN_F1
#define INP_2   PIN_F2
#define LED_0   PIN_B0
#define LED_1   PIN_B1

// TODO: Define any globals here
int input0;
int input1; 
int input2; 
int value;

char lcd_txt[32];
void write_lcd(int value){
	sprintf(lcd_txt,"%d",value);
   	int lcd_txt_length = strlen(lcd_txt);
   	lcd_write_at(2,0,lcd_txt_length, lcd_txt);
}

void main (void)
{   
	lcd_init();					// initialize lcd communication (SPI)
	memset(lcd_txt,'\0', 32);      // clear the serial buffer

	
  
  //Settings for LED and general I/O
	set_tris_b(0);
	set_tris_f(0xff);
	output_b(0);
	output_f(0)	;

	setup_port_a(ALL_ANALOG);
	setup_adc(ADC_CLOCK_INTERNAL);
	// Set all ADC pins to analog mode
	setup_adc_ports(ALL_ANALOG);
	set_adc_channel(2); 
	delay_us(10);


	while(1)
	{
		
		input0 = input(INP_0);
		input1 = input(INP_1);
		input2 = input(INP_2);
		
		if (input0)
			output_high(LED_0);
		else
			output_low(LED_0);	

		if (input1)
			output_high(LED_1);
		else 
			output_low(LED_1);

		if (input2 )
			output_toggle(TOGPIN);
		else
			output_low(TOGPIN);

		value = read_adc(); 	
    	write_lcd(value);
		printf("%d\n",value);
	

		
	}





  // 3A: TODO: Setup interrupts
	// Setup the push button
   	
	// Setup the timer (20ms)


	for(;;){
    // LOOP FOREVER control loop. 
    // TODO: Fill in loop behavior
  }
	
}	

// Interrupt handler for the button press
#INT_EXT1
void INT_EXT()
{
  // TODO: FILL ME IN
}

// Interrupt handler for the timer
#INT_TIMER3
void timer3()
{
  // TODO: FILL ME IN
}
