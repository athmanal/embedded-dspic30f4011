
	
	#include "project.h"
	
	#include "lcd_utils.h"
	
	#include <stdio.h>
	#include <string.h>
	
	#use delay( internal=7.37mhz)
	#fuses nowdt, noprotect       //Turn off the Watch-Dog Timer and disable code protection
	#use rs232(baud=9600, PARITY=N, STOP=1, UART1A)
	#use standard_io(b)
	#use standard_io(f)
	#use standard_io(e)
	
	// Convenience defines
	#define TOGPIN PIN_B3
	#define INP_0   PIN_F0
	#define INP_1   PIN_F1
	#define INP_2   PIN_F2
	#define LED_0   PIN_B0
	#define LED_1   PIN_B1
	
	// TODO: Define any globals here
	int input0;
	int input1; 
	int input2; 
	int VALUE;
	
	char lcd_txt[32];
	void write_lcd(int value){
	   sprintf(lcd_txt,"%d",value);
	      int lcd_txt_length = strlen(lcd_txt);
	      lcd_write_at(2,0,lcd_txt_length, lcd_txt);
	}
	
	
	void main (void)
	{   
	VALUE=0;
	enable_interrupts(INTR_GLOBAL);
	enable_interrupts(INT_EXT1);
	enable_interrupts(INT_EXT0);
	enable_interrupts(INT_TIMER2);
	setup_timer2(TMR_INTERNAL,0x46b4);


//Settings for LED and general I/O
   set_tris_b(0);
   set_tris_f(0xff);
   output_b(0);
   output_f(0)   ;


	lcd_init();               // initialize lcd communication (SPI)
	memset(lcd_txt,'\0', 32);      // clear the serial buffer
	setup_port_a(ALL_ANALOG);
	setup_adc(ADC_CLOCK_INTERNAL);
	   // Set all ADC pins to analog mode
	setup_adc_ports(ALL_ANALOG);
	set_adc_channel(2); 
	delay_us(10);
	int value;
		while(1)
	   {           
	         value=0;
	         spi_write(" ");    
	         //write_lcd(VALUE);
	       // delay_ms(5);  
	   }
	   
	}   
	
	
	#INT_TIMER2
	void timer_2()
	{
	  input0 = input(INP_0);
      input1 = input(INP_1);
      input2 = input(INP_2);
      
      if (input0)
         output_high(LED_0);
      else
         output_low(LED_0);   

      if (input1)
         output_high(LED_1);
      else 
         output_low(LED_1);

      if (input2 )
         output_toggle(TOGPIN);
      else
         output_low(TOGPIN);
	//delay_ms(500);
	clear_interrupt(INT_TIMER2);
	}
	
	#INT_EXT1
	void INT_EXT_1()
	{
	int value;
	value = read_adc();
	write_lcd(value);
	delay_ms(500);
	}
	
	#INT_EXT0
	void INT_EXT_0()
	{
	int value;
	value = read_adc();
	write_lcd(value);
	delay_ms(500);
	}