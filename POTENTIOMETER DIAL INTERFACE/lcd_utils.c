#include "lcd_utils.h"

#define LCDLINE1CMD     0x80    //Command to set LCD display to Line 1
#define LCDLINE2CMD     0xC0    //Command to set LCD display to Line 2

#use delay( internal=7.37mhz)

void lcd_init() 
{
	//NOTE: To program the board move switch 4 on S2 to ON
    //      to use the LCD, move switch 4 on S2 to OFF
	//configure SPI
	setup_spi(SPI_MASTER|SPI_CLK_DIV_16);	// set the SPI module as the bus master 
	delay_ms(2000);
	spi_write(LCDLINE1CMD);
	delay_ms(5);
}

//write a string at the current cursor position
unsigned int8 lcd_write(unsigned int8 str_length, char* str_pointer) 
{

	unsigned int8 n_char_wrt;	// # char written to the LCD
	n_char_wrt = 0;

	if (str_length == 0)
		return n_char_wrt;
	if (str_length > 32)	//cannot write more than 32 char on the LCD
		str_length = 32;
	
	
	for (n_char_wrt=0; n_char_wrt < str_length; n_char_wrt++) {
		spi_write(str_pointer[n_char_wrt]);
		delay_ms(5);		// wait 5ms before outputting the following char
	}

	if (str_length > 16) {
		spi_write(LCDLINE2CMD);
		delay_us(250);
		for (; n_char_wrt < str_length; n_char_wrt++) {
			spi_write(str_pointer[n_char_wrt]);
			delay_ms(5);		// wait 5ms before outputting the following char
		}
	}
	return n_char_wrt;

}

//write a single char at the given line and char position
//NOTE: char position goes from 0 to 15
unsigned int8 lcd_write_char_at(unsigned int8 line, unsigned int8 char_pos, unsigned int8 char_to_write) 
{
	unsigned int8 char_address;
	if ((line > 2) || (line == 0))
		return 0;
	if (char_pos > 15)
		return 0;
	if (line == 1)
		char_address = LCDLINE1CMD+char_pos;
	else
		char_address = LCDLINE2CMD+char_pos;
	
	//select address
	spi_write(char_address);
	delay_ms(5);		// wait 5ms before outputting the following char
	spi_write(char_to_write); //print char
	delay_ms(5);		// wait 5ms before outputting the following char

}

//write a string at the given line and char position
unsigned int8 lcd_write_at(unsigned int8 line, unsigned int8 char_pos, unsigned int8 str_length, char* str_pointer) 
{
		unsigned int8 n_char_wrt;	// # char written to the LCD
		unsigned int8 i;
		n_char_wrt = 0;
	
		if (str_length == 0)
			return n_char_wrt;
		if (str_length > 32)	//cannot write more than 32 char on the LCD
			str_length = 32;
		
		for (i=0; i < str_length; i++) {
			n_char_wrt = n_char_wrt + lcd_write_char_at(line, char_pos+i, str_pointer[i]);
		}
	
		return n_char_wrt;
}

