
#include "project.h"
#include "lcd_utils.h"

#include <stdio.h>
#include <string.h>

#use delay( internal=7.37mhz)
#fuses nowdt, noprotect       //Turn off the Watch-Dog Timer and disable code protection
#use rs232(baud=9600, PARITY=N, STOP=1, UART1A)
#use standard_io(b)
#use standard_io(f)
#use standard_io(e)

// Convenience defines
#define DIR     PIN_B0

// TODO: Define any globals here 
long value;
long duty;
int interrupt0; 
int interrupt1; 

char clear[10] = {"         "};
char lcd_txt[32];

void write_lcd(long value){
	sprintf(lcd_txt,"%d",value);
   	int lcd_txt_length = strlen(lcd_txt);
   	lcd_write_at(2,0,lcd_txt_length, lcd_txt);
}

void clear_lcd(){
   	int lcd_txt_length = strlen(clear);
   	lcd_write_at(2,0,lcd_txt_length, clear);
}

void main (void)
{   set_tris_b(0);
	//ENABLE INTERRUPTS
	enable_interrupts(INTR_GLOBAL);
	enable_interrupts(INT_EXT1);
	enable_interrupts(INT_EXT0);

	//INITIALIZE LCD
	lcd_init();					// initialize lcd communication (SPI)
	memset(lcd_txt,'\0', 32);      // clear the serial buffer

	//SETUP ADC CHANNEL
   	setup_port_a(ALL_ANALOG);
   	setup_adc(ADC_CLOCK_INTERNAL);
   	setup_adc_ports(ALL_ANALOG);
  	set_adc_channel(2); 
 	delay_us(10);

	//SETUP PWM

	setup_timer2(TMR_INTERNAL,0x3ff);
	setup_compare(3, COMPARE_PWM );

	while(1)  
	    {    	
			duty = read_adc();         
	  		write_lcd(duty);
			delay_ms(150);
			clear_lcd();
	
	  		if (interrupt0)        
	  		{        
				//TOGGLE PWM 
				set_pwm_duty(3,duty );
			}
			else
			{
				set_pwm_duty(3,0);    	
			}

		 	if (interrupt1)        
	  		{        
				//TOGGLE DIRECTION BIT
				output_high(DIR);
				
			}
			else
			{
				output_low(DIR);
			}
	
	
	
	      }

}

   #INT_EXT1
   void INT_EXT_1()
   {
if (interrupt1 ==0)
      interrupt1 = 1;
else
      interrupt1 = 0;
   }
   
   #INT_EXT0
   void INT_EXT_0()
   {
      
if (interrupt0 ==0)
      interrupt0 = 1;
else
      interrupt0 = 0;
   }