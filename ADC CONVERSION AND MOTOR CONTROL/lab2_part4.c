
#include "project.h"
#include "lcd_utils.h"

#include <stdio.h>
#include <string.h>

#use delay( internal=7.37mhz)
#fuses nowdt, noprotect       //Turn off the Watch-Dog Timer and disable code protection
#use rs232(baud=9600, PARITY=N, STOP=1, UART1A)
#use standard_io(b)
#use standard_io(f)
#use standard_io(e)

// Convenience defines
#define DIR     PIN_B0

// TODO: Define any globals here 
long duty;
int interrupt0;  
int interrupt1;
long prevQuadcount;
long quadcount;
long diff; 
int rpm;
long ticksPerMin;
long dutyCycle;
int interruptPeriod = 0x46b4;
int pwmPeriod = 0x3ff; 
int count; 

int rpmArray[25]; 
int dutyCycleArray[25];

char clear[10] = {"         "};
char lcd_txt[32];

void write_lcd_rpm(int value){
   sprintf(lcd_txt,"RPM = %d\r\n",value);
      int lcd_txt_length = strlen(lcd_txt);
      lcd_write_at(1,0,lcd_txt_length, lcd_txt);
}

void write_lcd_dutyCycle(int value){
   sprintf(lcd_txt,"DUTY = %d\r\n",value);
      int lcd_txt_length = strlen(lcd_txt);
      lcd_write_at(2,0,lcd_txt_length, lcd_txt);
}

void clear_lcd(){
      int lcd_txt_length = strlen(clear);
      lcd_write_at(2,0,lcd_txt_length, clear);
lcd_write_at(1,0,lcd_txt_length, clear);
}

void main (void)
{   
   //Setup I/O
   set_tris_b(0x00);

   //   ENABLE INTERRUPTS
   enable_interrupts(INTR_GLOBAL);
   enable_interrupts(INT_EXT1);
   enable_interrupts(INT_EXT0);

   //INITIALIZE LCD
   lcd_init();               // initialize lcd communication (SPI)
   memset(lcd_txt,'\0', 32);      // clear the serial buffer

      //SETUP ADC CHANNEL;
    setup_adc(ADC_CLOCK_INTERNAL);
    setup_adc_ports(sAN1 | sAN2 );
    set_adc_channel(2); 
    delay_us(10);

      //SETUP QEI
    setup_qei(QEI_MODE_X4|QEI_TIMER_INTERNAL,QEI_FILTER_DIV_1,QEI_FORWARD);
    quadcount = 0;
    prevQuadcount = 0;

	//SETUP PWM
	setup_timer2(TMR_INTERNAL,pwmPeriod);
   	setup_compare(3, COMPARE_PWM );

	count = 0; 

      while(count <25)  
          {  
			//PWM 
			duty = read_adc(); 
			set_pwm_duty(3,duty );
		//	printf(" Duty  = %ld  \r\n", duty); 
			dutyCycle = ((duty*100)/(1024)); 
		//	printf(" Dutycycle  = %ld  \r\n", dutyCycle); 
//delay_ms(500);
			//RPM
			prevQuadcount = quadcount;
      		quadcount = qei_get_count();// Read the count. 
	  		diff = quadcount - prevQuadcount;
      		ticksPerMin = abs(diff)/0.000333333;
	  		rpm = ticksPerMin/2048;

			//Interrupt
			if (interrupt0) 
			{
				clear_lcd();
				write_lcd_dutyCycle(dutyCycle);
				write_lcd_rpm(rpm);
				rpmArray[count] = rpm; 
				dutyCycleArray[count] = dutyCycle;
				count++;
				interrupt0 = 0; 
			}

			if (interrupt1)        
           	{        
            	//TOGGLE DIRECTION BIT
            	output_high(DIR);
            }
         	else
         	{
            	output_low(DIR);
         	}

		}

		set_pwm_duty(3,0);
		for (int i = 0; i < 25; i++)
		{
			printf(" %d Duty Cycle = %d  RPM = %d \r\n", i,dutyCycleArray[i], rpmArray[i]); 
		}
			
		


}


   #INT_EXT0
   void ext0()
   {
   	 interrupt0 = 1; 
   }

   #INT_EXT1
   void INT_EXT_1()
   {
	if (interrupt1 ==0)
      interrupt1 = 1;
	else
      interrupt1 = 0;
   }